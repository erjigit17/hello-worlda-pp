//
//  ContentView.swift
//  HelloWorldApp
//
//  Created by erjigit on 1/10/20.
//  Copyright © 2020 erjigit. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
